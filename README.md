This is an implementation of a URL Shortener MicroService that processes 
the a url entered by a user to convert it to a short url stored in a
persistant Database.

It is built on the .NET Core platform and the database is a MongoDB document
store.

The live demo is hosted on Azure at:
http://zx-fcc-urlshortener.azurewebsites.net/