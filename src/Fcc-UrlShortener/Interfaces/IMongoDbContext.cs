﻿using Fcc_UrlShortener.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fcc_UrlShortener.Interfaces
{
    public interface IMongoDbContext
    {
        IMongoCollection<UrlModel> ShortUrls { get; }
    }
}
