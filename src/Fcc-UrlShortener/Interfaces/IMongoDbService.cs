﻿using Fcc_UrlShortener.Models;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fcc_UrlShortener.Interfaces
{
    public interface IMongoDbService
    {
        string AddUrlModel(UrlModel model);
        UrlModel GetUrlModel(string key);
    }
}
