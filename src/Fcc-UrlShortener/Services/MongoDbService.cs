﻿using Fcc_UrlShortener.Interfaces;
using Fcc_UrlShortener.Models;
using MongoDB.Bson;
using MongoDB.Driver;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Hosting;

namespace Fcc_UrlShortener.Services
{
    public class MongoDbService : IMongoDbService
    {
        protected readonly IMongoDbContext _db;
        protected readonly IHostingEnvironment _env;

        public MongoDbService(IMongoDbContext db, IHostingEnvironment env)
        {
            _db = db;
            _env = env;
        }

        public string AddUrlModel(UrlModel model)
        {
            string key;
            FilterDefinition<UrlModel> filter;

            do
            {
                key = RandomizerUtil.GetRandomId();
                filter = Builders<UrlModel>.Filter.Eq("Key", key);
            }
            while (_db.ShortUrls.Find(filter).FirstOrDefault() != null);

            model.Key = key;
            model.ShortUrl = "http://localhost:51966/" + key;

            _db.ShortUrls.InsertOne(model);

            return $"{{ \"original_url\": \"{model.OriginalUrl}\", \"short_url\": \"{model.ShortUrl}\" }}";
        }

        public UrlModel GetUrlModel(string key)
        {
            var filter = Builders<UrlModel>.Filter.Eq("Key", key);
            return _db.ShortUrls.Find(filter).FirstOrDefault();
        }

    }
}
