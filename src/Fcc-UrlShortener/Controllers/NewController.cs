﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Fcc_UrlShortener.Services;
using Fcc_UrlShortener.Interfaces;
using MongoDB.Bson;
using Fcc_UrlShortener.Models;
using Newtonsoft.Json;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Fcc_UrlShortener.Controllers
{
    [Route("[controller]")]
    public class NewController : Controller
    {
        protected readonly IMongoDbService _db;

        public NewController(IMongoDbService db)
        {
            _db = db;
        }

        // GET: api/values
        public IActionResult Get(string url)
        {
            UrlModel doc = new UrlModel {
                OriginalUrl = url
            };
            
            return Content(_db.AddUrlModel(doc), "application/json");
        }
    }
}
