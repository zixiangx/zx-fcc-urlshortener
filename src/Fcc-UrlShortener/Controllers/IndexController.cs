﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Fcc_UrlShortener.Interfaces;
using Fcc_UrlShortener.Models;
using Newtonsoft.Json;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Fcc_UrlShortener.Controllers
{
    [Route("/")]
    public class IndexController : Controller
    {
        protected readonly IMongoDbService _db;

        public IndexController(IMongoDbService db)
        {
            _db = db;
        }

        [HttpGet]
        // GET: api/values
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("{shortUrl}")]
        // GET: api/values
        public IActionResult Index(string shortUrl)
        {
            UrlModel model = _db.GetUrlModel(shortUrl);

            if (model != null)
                return Redirect(model.OriginalUrl);

            return Content("{ \"error\": \"This URL is not on the database\" }", "application/json");
        }
    }
}
