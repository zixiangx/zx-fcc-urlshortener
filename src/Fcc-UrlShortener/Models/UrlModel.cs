﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fcc_UrlShortener.Models
{
    public class UrlModel
    {
        public ObjectId _id { get; set; }
        public string Key { get; set; }
        public string ShortUrl { get; set; }
        public string OriginalUrl { get; set; }
    }
}
