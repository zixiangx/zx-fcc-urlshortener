﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Fcc_UrlShortener.Interfaces;
using Microsoft.Extensions.Options;
using Fcc_UrlShortener.Models.Settings;
using MongoDB.Driver;
using MongoDB.Bson;

namespace Fcc_UrlShortener.Models
{
    public class MongoDbContext : IMongoDbContext
    {
        MongoClient Client;
        IMongoDatabase Database;

        public MongoDbContext(IOptions<MongoSettings> mongoSettings)
        {
            Client = new MongoClient(mongoSettings.Value.ConnectionString);
            Database = Client.GetDatabase(mongoSettings.Value.DatabaseName);
        }

        public IMongoCollection<UrlModel> ShortUrls
        {
            get
            {
                return Database.GetCollection<UrlModel>("ShortUrls");
            }
        }
    }
}
